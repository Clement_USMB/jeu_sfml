#include "def_fonction.hpp"
// fonction pour initiaiser des variable de types texte
Text initialisationtexte (float taille,int positiony, char nom [], Font &font)
{
    Text texte ;

    texte.setFont(font);
    texte.setColor(Color::White);
    texte.setStyle(Text::Bold);
    texte.setCharacterSize(taille);
    texte.setString(nom) ;
    texte.setPosition((LARGEUR_FENETRE/2)-((texte.getGlobalBounds().width)/2),positiony) ;

    return texte ;
}
//fonction pour sélectionner une option dans un menu
void positionmenu (int &i, int nb1, Text tab [])
{
    tab[i+1].setColor(Color::White); // l'ancienne option sur laquelle etait le joueur est remise en blanc
    i = (i+3+nb1)%3;
    tab[i+1].setColor(Color::Red); // la nouvelle option sur laquelle est le joueur passe en rouge
}
// menu principale
void gestionmenu (RenderWindow &fenetre,RenderWindow &fenetre2, RenderWindow &fenetre3,RenderWindow &fenetre4,Text tab[],Sprite image, int &i,Event event)
{
    float modiftaille = LARGEUR_FENETRE/800.0;

    tab[i+1].setColor(Color::Red); // l'option sur laquelle est actuellement le joueur est en rouge

    tab[i].setColor(Color::Black);

    SoundBuffer click;
    click.loadFromFile(SOURIS);
    Sound soundClick;
    soundClick.setBuffer(click);

    while (fenetre.isOpen())
    {
        // gestion évenements ;
        while (fenetre.pollEvent(event))
        {
            //fermer la fenetre
            if (event.type == Event::Closed)
                fenetre.close();

            if (event.type == Event::KeyPressed)
            {

                soundClick.play();

                if (event.key.code == Keyboard::Up)
                {
                    positionmenu (i,(-1),tab) ;
                }

                if (event.key.code == Keyboard::Down)
                {
                    positionmenu (i,1,tab) ;
                }
                if (event.key.code == Keyboard::Return)
                {
                    if (i==0)
                    {
                        fenetre4.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Get out !");
                        fenetre.close();

                    }
                    if (i==1)
                    {
                        fenetre3.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Choix de la difficulte");
                        fenetre.close();
                    }

                    if (i==2)
                    {
                        fenetre2.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Commandes");
                        fenetre.close();
                    }

                }
                if (event.key.code == Keyboard::Q)
                {
                    fenetre.close();
                    i = -1;

                }
            }

        }

        fenetre.clear() ;

        fenetre.draw(image);

        for(int t = 0 ; t <TABLEAU_MENU_P; t++) // affichage de tous les textes
            fenetre.draw(tab[t]);

        fenetre.display();
    }
}
// menu commandes
void gestionmenuc (RenderWindow &fenetre,RenderWindow &fenetre2, Text tab[],Sprite image,Event event)
{
    tab[4].setColor(Color::Black);

    while (fenetre.isOpen())
    {
        while (fenetre.pollEvent(event))
        {
            if (event.type == Event::KeyPressed)
            {
                if (event.key.code == Keyboard::R)
                {
                    fenetre2.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Menu principale");
                    fenetre.close();
                }
            }
        }
        fenetre.clear() ;

        fenetre.draw(image);

        for(int t = 0 ; t <TABLEAU_MENU_C ; t++) // affichage de tous les textes
            fenetre.draw(tab[t]);

        fenetre.display();
    }
}
// menu des difficultees
void gestionmenud (RenderWindow &fenetre,RenderWindow &fenetre2, Text tab[],Sprite image, int &i,Event event, int &nvdifficulte)
{

    tab[0].setColor(Color::Black);

    while (fenetre.isOpen())
    {
        tab[i+1].setColor(Color::Red);

        while (fenetre.pollEvent(event))
        {
            if (event.type == Event::KeyPressed)
            {
                if (event.key.code == Keyboard::Up)
                {
                    positionmenu (i,(-1),tab) ;
                }

                if (event.key.code == Keyboard::Down)
                {
                    positionmenu (i,1,tab) ;
                }
                if (event.key.code == Keyboard::Return)
                {
                    if (i==0)
                        nvdifficulte = 1;


                    if (i==1)
                        nvdifficulte = 2;


                    if (i==2)
                        nvdifficulte = 3;

                }
                if (event.key.code == Keyboard::R)
                {
                    fenetre2.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Menu principale");
                    fenetre.close();

                }
            }

        }


        fenetre.clear() ;

        fenetre.draw(image);

        for(int t = 0 ; t <TABLEAU_MENU_D ; t++)
            fenetre.draw(tab[t]);

        fenetre.display();

    }

}
// lancement de la partie
void gestionmenudp (RenderWindow &app,RenderWindow &fenetre2,RenderWindow &fenetre3,RenderWindow &fenetre4,int nvdifficulte)
{
    //variables
    bool pris = 0;
    bool pris2 = 0;
    Vector2f previous;
    Aventurier aventurier;
    float modiftaille = LARGEUR_FENETRE/800.0; // pour modifier la taille du texte en fonction de la page
    char score [50]  = "Vous avez mit : " ;
    char record [50] = "Le record a battre est de : " ;

    //gestion des scores
    int minutes = 0;
    int secondes = 0;

    FILE * file = fopen("scoresN1.txt", "r");

    fscanf(file, "%d:%d", &minutes, &secondes);
    sprintf(record,"%s%d minute(s) %d seconde(s)",record,minutes,secondes);

    fclose(file);

    //gestion des FPS
    app.setFramerateLimit(FPS);

    //Pour le découpage de l'image du joueur
    enum Dir {Down, Left, Right, Up};
    Vector2i animation(1, Down);

    //Pour le changement d'image du joueur qui se déplace
    bool updateFPS =false;

    Sprite spriteAventurier,
           pomme,
           pommePoison;

    //Charger et nommer les sons dans le programme


    //On declare qu'il y a du son
    //(soundBackground pour le vent et sound pour les ev�nements)

//son

    SoundBuffer flechette;
    flechette.loadFromFile(FLECHE);
    Sound soundFlechette;
    soundFlechette.setVolume(30);
    soundFlechette.setBuffer(flechette);

    SoundBuffer fin;
    fin.loadFromFile(FEUX_ARTIFICE);
    Sound soundFin;
    soundFin.setVolume(30);
    soundFin.setBuffer(fin);

    /* SoundBuffer sonKamikaze;
    if (!sonKamikaze.loadFromFile(KAMI))
        printf("PB de chargement du son %s !\n", KAMI);

    SoundBuffer sonMonstreSimple;
    if (!sonMonstreSimple.loadFromFile(MONSTRES))
        printf("PB de chargement du son %s !\n", MONSTRES);
    */


    SoundBuffer bufferVent;
    bufferVent.loadFromFile("Vent.wav");
    Sound soundVent;
    soundVent.setBuffer(bufferVent);
    soundVent.play();
    soundVent.setVolume(20);
    soundVent.setLoop(true);


    //Donner texture au joueur
    Texture perso,
            pommeT,
            murT,
            pommePoisonT;
    perso.loadFromFile(AVENTURIER);
    pommeT.loadFromFile(POMME);
    murT.loadFromFile(BRIQUE);
    pommePoisonT.loadFromFile(POMME_POISON);

    perso.setSmooth(true);
    pommeT.setSmooth(true);

    spriteAventurier.setTexture(perso);
    pomme.setTexture(pommeT);
    pommePoison.setTexture(pommePoisonT);
    spriteAventurier.setPosition(APPARITION_X_AVENTURIER,APPARITION_Y_AVENTURIER);

    //Les chronomètres pour le déplacement du perso et pour le chrono du jeu
    Clock clock,  //gestion temps réel
          chrono, //chrono score
          time; //animation perso

    Time tempsEcoule,  //gestion temps réel
         tempsChrono = chrono.getElapsedTime(); // chrono score

    float dt = 0, //gestion temps reél
          scoreChrono = tempsChrono.asSeconds(); // chrono score

    //tableaux
        //pour la map
            int labyPetitMap[TAILLE_TABLEAU_Y][TAILLE_TABLEAU_X] =
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,4,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
        {1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,1,1,1,3,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,1,1,1,0,1,1,3,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,1},
        {1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,5,1},
        {1,0,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,3,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    std::vector<RectangleShape>vecLaby;
    std::vector<RectangleShape>vecMur;
    std::vector<RectangleShape>vecFin;
    std::vector<RectangleShape>vecPiege;
    for(int y = 0; y<TAILLE_TABLEAU_Y; y++)
    {
        for(int x = 0; x<TAILLE_TABLEAU_X; x++)
        {
            if(labyPetitMap[y][x] == 1)
            {
                RectangleShape mur(Vector2f(TAILLE_MUR,TAILLE_MUR));
                mur.setFillColor(Color(230,134,24));
                mur.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                mur.setTexture(&murT);
                vecLaby.push_back(mur);
                vecMur.push_back(mur);
            }
            else if(labyPetitMap[y][x] == 0)
            {
                RectangleShape sol(Vector2f(TAILLE_MUR,TAILLE_MUR));
                sol.setFillColor(Color(10,162,28));
                sol.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                vecLaby.push_back(sol);
            }
            else if(labyPetitMap[y][x] == 2)
            {
                RectangleShape fin(Vector2f(TAILLE_MUR,TAILLE_MUR));
                fin.setFillColor(Color(10,162,28));
                fin.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                vecFin.push_back(fin);
                vecLaby.push_back(fin);
            }
            else if(labyPetitMap[y][x] == 3)
            {
                RectangleShape piege(Vector2f(TAILLE_MUR,TAILLE_MUR));
                piege.setFillColor(Color(10,153,26));
                piege.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                vecPiege.push_back(piege);
                vecLaby.push_back(piege);
            }
            else if(labyPetitMap[y][x] == 4)
            {
                RectangleShape sol(Vector2f(TAILLE_MUR,TAILLE_MUR));
                sol.setFillColor(Color(10,162,28));
                sol.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                pomme.setPosition(x*TAILLE_MUR+TAILLE_MUR/2,y*TAILLE_MUR+TAILLE_MUR/4);
                vecLaby.push_back(sol);
            }
            else if(labyPetitMap[y][x] == 5)
            {
                RectangleShape sol(Vector2f(TAILLE_MUR,TAILLE_MUR));
                sol.setFillColor(Color(10,162,28));
                sol.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                pommePoison.setPosition(x*TAILLE_MUR+TAILLE_MUR/4,y*TAILLE_MUR+TAILLE_MUR/2);
                vecLaby.push_back(sol);
            }
        }
    }
    // tableaux pour le texte
    Text textevictoire [TABLEAU_FENETRE_FINAL] ; // pour la fenetre de victoire
    Text textedefaite [TABLEAU_FENETRE_FINAL]; // pour la fenetre de defaite

    //parti initialisation des textes
    //le font
    Font fontmenu;
    fontmenu.loadFromFile(POLICE_ECRITURE);
    //initialisation des textes de la fenetre de victoire
    textevictoire[0] = initialisationtexte ((25*modiftaille),20,VICTOIRE,fontmenu) ;
   // textevictoire[1] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2),score,fontmenu) ;
    textevictoire[2] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2)+100,record,fontmenu) ;
    textevictoire[3] = initialisationtexte((20*modiftaille),(HAUTEUR_FENETRE-30),RECOMMENCER,fontmenu);

    //initialisation des textes de la fenetre de defaite
    textedefaite[0] = initialisationtexte ((25*modiftaille),20,DEFAITE,fontmenu) ;
    textedefaite[1] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2),ENCOURAGEMENT,fontmenu) ;
    textedefaite[2] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2)+100,record,fontmenu) ;
    textedefaite[3] = initialisationtexte((20*modiftaille),(HAUTEUR_FENETRE-30),RECOMMENCER,fontmenu);

    //les images
    //image pour la fenetre de victoire
    Texture imagefinal;
    if (!imagefinal.loadFromFile(IMAGE_VICTOIRE))
        printf("Probleme de chargement de l'image de victoire");
    Sprite imagevictoire(imagefinal);
    imagevictoire.setScale(LARGEUR_FENETRE/imagefinal.getSize().x,HAUTEUR_FENETRE/imagefinal.getSize().y);

    //image pour la fenetre de defaite
    Texture imagefinal2;
    if(! imagefinal2.loadFromFile(IMAGE_DEFAITE))
        printf("probleme de chargement de l'image de defaite");
    Sprite imagedefaite(imagefinal2);
    imagedefaite.setScale(LARGEUR_FENETRE/imagefinal2.getSize().x,HAUTEUR_FENETRE/imagefinal2.getSize().y);


        while (app.isOpen())
    {
        // Process events
        Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
            // Escape pressed : exit
            if (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
                app.close();
            //Le perso ne change pas si aucune touche pressée
            if(event.type == Event::KeyPressed)
                updateFPS = true;
            else
                updateFPS = false;
        }
        //}

        previous.x = aventurier.p.x;
        previous.y = aventurier.p.y;

        FloatRect spriteJoueur = spriteAventurier.getGlobalBounds();
        if (Keyboard::isKeyPressed( Keyboard::Left))
        {
            animation.y=Left;
            Vector2f test;
            test.x = aventurier.p.x+TAILLE_AVENTURIER;
            test.y = aventurier.p.y;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y][x-1] != 1){
                aventurier.p.x -= SPEED_AVENTURIER;
            }
        }

        if (Keyboard::isKeyPressed( Keyboard::Right))
        {
            animation.y=Right;
            Vector2f test;
            test.x = aventurier.p.x-TAILLE_AVENTURIER;
            test.y = aventurier.p.y;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y][x+1] != 1){
                aventurier.p.x += SPEED_AVENTURIER;
            }

        }

        if (Keyboard::isKeyPressed( Keyboard::Up))
        {
            animation.y=Up;

            Vector2f test;
            test.x = aventurier.p.x;
            test.y = aventurier.p.y+TAILLE_AVENTURIER;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y-1][x] != 1){
                aventurier.p.y -= SPEED_AVENTURIER;
            }
        }

        if (Keyboard::isKeyPressed( Keyboard::Down))
        {
            animation.y=Down;
            Vector2f test;
            test.x = aventurier.p.x;
            test.y = aventurier.p.y-TAILLE_AVENTURIER;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y+1][x] != 1){
                aventurier.p.y += SPEED_AVENTURIER;
            }

        }
        //Pour que le decoupage de l'image soit bien
        //Et donne impression que le perso se deplace
        if (updateFPS==true)
        {
            if (time.getElapsedTime().asMilliseconds() >=70)
            {
                animation.x--;
                if (animation.x*TAILLE_AVENTURIER >= perso.getSize().x)
                    animation.x =2;
                time.restart();
            }
        }
        //Pour regeler qu'elle image est prise
        spriteAventurier.setTextureRect(IntRect(animation.x*TAILLE_AVENTURIER, animation.y*TAILLE_AVENTURIER, TAILLE_AVENTURIER, TAILLE_AVENTURIER));
        spriteAventurier.setPosition(aventurier.p.x,aventurier.p.y);


        //Mesure en temps reel
        tempsEcoule = clock.restart();
        dt = tempsEcoule.asSeconds();

        //draw de la barre de vie
        //Barre de vie

        RectangleShape overlayBarreVie(Vector2f(50,20));
        overlayBarreVie.setPosition(aventurier.p.x,aventurier.p.y-60);
        overlayBarreVie.setFillColor(Color::White);
        RectangleShape fondOverlay(Vector2f(40,10));
        fondOverlay.setPosition(aventurier.p.x+5,aventurier.p.y-55);
        fondOverlay.setFillColor(Color::Black);
        RectangleShape barreVie(Vector2f(aventurier.pv,10));
        barreVie.setPosition(aventurier.p.x+5,aventurier.p.y-55);
        barreVie.setFillColor(Color::Green);




        if(aventurier.pv<=20)
            barreVie.setFillColor(Color(222,120,31));
        if(aventurier.pv<=10)
            barreVie.setFillColor(Color::Red);

        if(!vecPiege.empty())
        {
            for(int i = 0; i < vecPiege.size(); i++)
            {
                if(spriteJoueur.intersects(vecPiege[i].getGlobalBounds()))
                {
                    soundFlechette.play();
                    aventurier.pv -= 10;
                    vecPiege.erase(vecPiege.begin()+i);
                }
            }

        }
        if(spriteJoueur.intersects(pomme.getGlobalBounds()))
        {
            pomme.setPosition(1500,0);
            if(aventurier.pv<=35)
            {
                aventurier.pv=aventurier.pv+5;
            }
            else
            {
                aventurier.pv=40;
            }

        }
        if(spriteJoueur.intersects(pommePoison.getGlobalBounds()))
        {
            pommePoison.setPosition(1500,0);
            aventurier.pv -= 20;

        }


        //détéection de l'arrivée
        if(spriteJoueur.intersects(vecFin[0].getGlobalBounds()))
        {
           // soundVent.stop();
            //if(soundFin.getStatus() == SoundSource::Stopped)
           // {
           //     soundFin.play();
           // }

            soundFin.play(); //pour lancer le son
            fenetre3.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Gagner !");
            fenetrefinal (fenetre3,fenetre2,textevictoire,imagevictoire,event);
            app.close();

            //mettre la fin
        }
        else if (aventurier.pv <= 0)
        {
            fenetre4.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Perdu ...");
            fenetrefinal (fenetre4,fenetre2,textedefaite,imagedefaite,event);
            app.close();
        }

        // Clear screen
        app.clear();

        // Draw the sprite

        for(int i=0; i < vecLaby.size(); i++)
        {
            app.draw(vecLaby[i]);
        }

        View view1(Vector2f(aventurier.p.x,aventurier.p.y),Vector2f(300.f, 200.f));
        app.setView(view1);
        app.draw(pomme);
        app.draw(pommePoison);
        app.draw(spriteAventurier);
        app.draw(overlayBarreVie);
        app.draw(fondOverlay);
        app.draw(barreVie);
        printf("%d minutes et %d secondes\n", minutes, secondes);
        // Update the window
        app.display();
    }
}
// fenetre victoire et defaite
void fenetrefinal (RenderWindow &fenetre,RenderWindow &fenetre2, Text tab[], Sprite image,Event event)
{
    while (fenetre.isOpen())
    {
        while (fenetre.pollEvent(event))
        {
            if (event.type == Event::KeyPressed)
            {
                if (event.key.code == Keyboard::Return)
                {
                    fenetre2.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Menu principale");
                    fenetre.close();
                }
            }

        }



        fenetre.clear() ;

        fenetre.draw(image);

        for(int t = 0 ; t <TABLEAU_FENETRE_FINAL ; t++)
            fenetre.draw(tab[t]);

        fenetre.display();
    }
}
