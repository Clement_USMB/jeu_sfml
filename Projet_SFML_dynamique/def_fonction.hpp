#ifndef DEF_FONCTION_HPP_INCLUDED
#define DEF_FONCTION_HPP_INCLUDED
#include <stdio.h>
#include <stdlib.h>


#include <SFML/Graphics.hpp>

// constantes pour les fen�tres
#define HAUTEUR_FENETRE 600.0
#define LARGEUR_FENETRE 800.0

// constantes pour les textes du menu principale
#define NOM "Get out !"
#define DIFFICULTES  "Difficultees"
#define COMMANDES "Commandes"
#define DEBUT_PARTI "Commencer une partie"
#define CONFIRMER "Confirmer : entrer"
#define QUITTER "Quitter le jeu : q"

// constantes pour les textes du menu des commandes
#define DEP_HAUT "Aller vers le haut : fleche du haut"
#define DEP_BAS "Aller vers le bas : fleche du bas"
#define DEP_GAUCHE "Aller vers la gauche : fleche de gauche"
#define DEP_DROITE "Aller vers la droite : fleche de droite"
#define ATTAQUE "Attaquer : touche attaque"
#define SURPRISE "Prendre un bonus/un malus  : touche prendre"
#define RETOUR "Retour au menu principal : r"

// constantes pour les textes du menu des diffiicultees
#define DEFAUT "Choississez la difficulte (par defaut en facile) :"
#define FACILE "Facile : pas d'ennemis, petite carte et large champs de vision"
#define NORMALE "Normal : ennemis de force moyenne, carte moyenne et champs de vision reduit"
#define DIFFICILE "Difficile : ennemis tres fort, grande carte et champs de vision tres faible"

//constantes des textes de victoire
#define VICTOIRE "Felicitation vous vous etes echapper du labyrinthe !"
#define RECOMMENCER "Pour retourner sur le menu principal : touche entrer"

//constantes des textes de defaites
#define DEFAITE "Dommage vous etes mort dans le labyrinthe"
#define ENCOURAGEMENT "Courage ! Vous y arriverez la prochaine fois !"

//constantes pour le nombre de valeur dans les tableaux
#define TABLEAU_MENU_P 6
#define TABLEAU_MENU_C 7
#define TABLEAU_MENU_D 6
#define TABLEAU_FENETRE_FINAL 4

// constantes ressources sons et images
#define IMAGE_MENUP "ressources/labyrinthe.jpg"
#define IMAGE_VICTOIRE "ressources/victoire.jfif"
#define IMAGE_DEFAITE "ressources/defaite.jfif"
#define POLICE_ECRITURE "ressources/arial.ttf"

//constantes du jeu
#include <SFML/Audio.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#define TAILLE_TABLEAU_Y 16
#define TAILLE_TABLEAU_X 30
#define TAILLE_MUR 30

//Aventurier
#define SPEED_AVENTURIER 1
#define PV_AVENTURIER 40
#define DEGATS_AVENTURIER 25
#define RAYON 15
//Kamikaze
#define SPEED_KAMIKAZE 1
#define PV_KAMIKAZE 50
#define DEGATS_KAMIKAZE 50
//Monstre simple
#define SPEED_MONSTRESIMPLE 1
#define PV_MONSTRESIMPLE 150
#define DEGATS_MONSTRESIMPLE 25
//Apparitions
#define APPARITION_X_AVENTURIER 30
#define APPARITION_Y_AVENTURIER 420
//Les sons
#define KAMI "ressources/Kami.wav"
#define MONSTRES "ressources/MonstreS.wav"
#define YEAH "ressources/Yeah.wav"
#define VENT "Vent.wav"
#define FLECHE "ressources/fleche.wav"
#define FEUX_ARTIFICE "ressources/Feu_artifice.wav"
#define SOURIS "Click_souris.wav"

//Les images
#define AVENTURIER "ressources/aventurier.png"
#define POMME "ressources/pomme.png"
#define BRIQUE "ressources/brique.jpg"
#define POMME_POISON "ressources/pomme_poison.png"
#define TAILLE_AVENTURIER 21

//temps r�el
#define FPS 60

typedef struct
{
    int x=APPARITION_X_AVENTURIER;
    int y=APPARITION_Y_AVENTURIER;
} Position;

typedef struct
{
    int speed=SPEED_AVENTURIER;
    int pv=PV_AVENTURIER;
    int degats=DEGATS_AVENTURIER;
    Position p;
} Aventurier;

/* on peut les enlever :

typedef struct
{
    int speed=SPEED_KAMIKAZE;
    int pv=PV_KAMIKAZE;
    int degats=DEGATS_KAMIKAZE;
    Position p;
} Kamikaze;

typedef struct
{
    int speed=SPEED_MONSTRESIMPLE;
    int pv=PV_MONSTRESIMPLE;
    int degats=DEGATS_MONSTRESIMPLE;
    Position p;
} MonstreSimple;

*/

using namespace sf ;

//menu principale
void gestionmenu (RenderWindow &fenetre,RenderWindow &fenetre2,RenderWindow &fenetre3,RenderWindow &fenetre4, Text tab[],Sprite image, int &i,Event event);

//menu des commandes
void gestionmenuc (RenderWindow &fenetre,RenderWindow &fenetre2, Text tab[],Sprite image,Event event);

// menu des difficultees
void gestionmenud (RenderWindow &fenetre,RenderWindow &fenetre2, Text tab[],Sprite image, int &i,Event event, int &nvdifficulte);

// lancement de la partie
void gestionmenudp (RenderWindow &fenetre,RenderWindow &fenetre2,RenderWindow &fenetre3,RenderWindow &fenetre4,int nvdifficulte);

// fonction d'affichage de la fenetre de victoire et de defaite
void fenetrefinal (RenderWindow &fenetre,RenderWindow &fenetre2, Text tab[], Sprite Image,Event event);

//fonction pour initiaiser des variable de types texte
Text initialisationtexte (float taille,int positiony, char texte [], Font &font);

//fonction pour s�lectionner une option dans un menu
void positionmenu (int &i, int nb1, Text tab []);

#endif // DEF_FONCTION_HPP_INCLUDED
