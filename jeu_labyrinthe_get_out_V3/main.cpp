#include "def_fonction.hpp"
int main()
{
    //variables
    Event event;
    int i = 0 ;
    int nvdifficulte = 1 ; // facile : 1 normale : 2 difficile : 3
    float modiftaille = LARGEUR_FENETRE/800.0; // pour modifier la taille du texte en fonction de la page

    //tableaux
    Text textemenup [TABLEAU_MENU_P] ; //pour le menu principale
    Text textecommande [TABLEAU_MENU_C] ; // pour les commandes
    Text textedifficultee [TABLEAU_MENU_D]; // pour le choix des difficultees

    // fenetre menu prinsipale
    RenderWindow menup(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Menu principale");// menu principale
    RenderWindow menud; // menu des difficultees
    RenderWindow menuc; // menu des commandes
    RenderWindow menudp; //  debut de la partie
    RenderWindow victoire ; // fenetre victoire
    RenderWindow defaite ; // fentre defaite

    //initialisation du texte
    Font fontmenu;
    if (!fontmenu.loadFromFile(POLICE_ECRITURE))
        return EXIT_FAILURE ;

    //initialisation des textes du menu principale
    textemenup[0] = initialisationtexte ((60*modiftaille),20,NOM,fontmenu) ;
    textemenup[1] = initialisationtexte ((25*modiftaille),(HAUTEUR_FENETRE/2)-100,DEBUT_PARTI,fontmenu) ;
    textemenup[2] = initialisationtexte ((25*modiftaille),(HAUTEUR_FENETRE/2),DIFFICULTES,fontmenu) ;
    textemenup[3] = initialisationtexte ((25*modiftaille),(HAUTEUR_FENETRE/2)+100,COMMANDES,fontmenu) ;
    textemenup[4] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE-50),CONFIRMER,fontmenu) ;
    textemenup[5] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE-30),QUITTER,fontmenu) ;

    //initialisation des textes du menu des commandes
    textecommande[0] = initialisationtexte((25*modiftaille),(HAUTEUR_FENETRE/2 - 90),DEP_HAUT,fontmenu);
    textecommande[1] = initialisationtexte((25*modiftaille),(HAUTEUR_FENETRE/2 - 30),DEP_BAS,fontmenu);
    textecommande[2] = initialisationtexte((25*modiftaille),(HAUTEUR_FENETRE/2 + 30),DEP_GAUCHE,fontmenu);
    textecommande[3] = initialisationtexte((25*modiftaille),(HAUTEUR_FENETRE/2 + 90),DEP_DROITE,fontmenu);
    textecommande[4] = initialisationtexte((30*modiftaille),(30),COMMANDES,fontmenu);
    //textecommande[5] = initialisationtexte((25*modiftaille),(HAUTEUR_FENETRE/2 + 160),SURPRISE,fontmenu);
    textecommande[6] = initialisationtexte((20*modiftaille),(HAUTEUR_FENETRE-30),RETOUR,fontmenu);

    //initialisation des textes du choix des difficultees
    textedifficultee[0] = initialisationtexte ((25*modiftaille),30,DEFAUT,fontmenu) ;
    textedifficultee[1] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2)-100,FACILE,fontmenu) ;
    textedifficultee[2] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2),NORMALE,fontmenu) ;
    textedifficultee[3] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE/2)+100,DIFFICILE,fontmenu) ;
    textedifficultee[4] = initialisationtexte((20*modiftaille),(HAUTEUR_FENETRE-30),RETOUR,fontmenu);
    textedifficultee[5] = initialisationtexte ((20*modiftaille),(HAUTEUR_FENETRE-50),CONFIRMER,fontmenu) ;

    // initialisation des sons

    // initialisation de l'image
    Texture imagelabyrinthe;
    if (!imagelabyrinthe.loadFromFile(IMAGE_MENUP))
        return EXIT_FAILURE;
    Sprite imagemenu(imagelabyrinthe);
    imagemenu.setScale(LARGEUR_FENETRE/imagelabyrinthe.getSize().x,HAUTEUR_FENETRE/imagelabyrinthe.getSize().y);

    //d�but du jeu
    do
    {
        //menu principale
        gestionmenu (menup,menuc,menud,menudp,textemenup, imagemenu, i, event);

        //menu commandes
        gestionmenuc (menuc,menup,textecommande, imagemenu,event);

        //menu difficult�es
        gestionmenud (menud,menup,textedifficultee, imagemenu, i, event,nvdifficulte);

        //lancement de la partie
        gestionmenudp (menudp,menup,victoire,defaite,nvdifficulte);

    }
    while(!(i==-1));


    return EXIT_SUCCESS;
}
/*
        //d�t�ection de l'arriv�e
        if(spriteJoueur.intersects(vecFin[0].getGlobalBounds()))
        {
            fenetre3.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Gagner !");
            fenetrefinal (fenetre3,fenetre2,textevictoire,imagevictoire,event);
            app.close();

        }
        else if (aventurier.pv == 0)
        {
            fenetre4.create(VideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE), "Perdu ...");
            fenetrefinal (fenetre4,fenetre2,textevictoire,imagevictoire,event);
            app.close();
        }
        // les sons
        //son pour
        //SoundBuffer gagner;
       // if (!gagner.loadFromFile(""))
       // printf("Probleme de chargement du son pour");
       // Sound win;
       // win.setBuffer(gagner); // win.play(); pour lancer le son
*/



