#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#define TAILLE_TABLEAU_Y 16
#define TAILLE_TABLEAU_X 30
#define TAILLE_MUR 30

//Aventurier
#define SPEED_AVENTURIER 1
#define PV_AVENTURIER 40
#define DEGATS_AVENTURIER 25
#define RAYON 15
//Kamikaze
#define SPEED_KAMIKAZE 1
#define PV_KAMIKAZE 50
#define DEGATS_KAMIKAZE 50
//Monstre simple
#define SPEED_MONSTRESIMPLE 1
#define PV_MONSTRESIMPLE 150
#define DEGATS_MONSTRESIMPLE 25
//Apparitions
#define APPARITION_X_AVENTURIER 30
#define APPARITION_Y_AVENTURIER 420
//Les sons
#define KAMI "Kami.wav"
#define MONSTRES "MonstreS.wav"
#define YEAH "Yeah.wav"
#define VENT "Vent.wav"
#define FLECHE "fleche.wav"
#define FEUX_ARTIFICE "Feu_artifice.wav"

//Les images
#define AVENTURIER "aventurier.png"
#define POMME "pomme.png"
#define BRIQUE "brique.jpg"
#define POMME_POISON "pomme_poison.png"
#define TAILLE_AVENTURIER 21

//temps réel
#define FPS 60

using namespace sf;

typedef struct
{
    int x=APPARITION_X_AVENTURIER;
    int y=APPARITION_Y_AVENTURIER;
} Position;

typedef struct
{
    int speed=SPEED_AVENTURIER;
    int pv=PV_AVENTURIER;
    int degats=DEGATS_AVENTURIER;
    Position p;
} Aventurier;

typedef struct
{
    int speed=SPEED_KAMIKAZE;
    int pv=PV_KAMIKAZE;
    int degats=DEGATS_KAMIKAZE;
    Position p;
} Kamikaze;

typedef struct
{
    int speed=SPEED_MONSTRESIMPLE;
    int pv=PV_MONSTRESIMPLE;
    int degats=DEGATS_MONSTRESIMPLE;
    Position p;
} MonstreSimple;


int main()
{
    bool pris = 0;
    bool pris2 = 0;
    Vector2f previous;
    Aventurier aventurier;
    // Create the main window
    RenderWindow app(VideoMode(800, 600), "SFML window");
    app.setFramerateLimit(FPS);



    //Pour le découpage de l'image du joueur
    enum Dir {Down, Left, Right, Up};
    Vector2i animation(1, Down);

    //Pour le changement d'image du joueur qui se déplace
    bool updateFPS =false;

    Sprite spriteAventurier,
           pomme,
           pommePoison;

    //Charger et nommer les sons dans le programme


    //On declare qu'il y a du son
    //(soundBackground pour le vent et sound pour les ev�nements)

//son
    SoundBuffer flechette;
    flechette.loadFromFile(FLECHE);
    Sound soundFlechette;
    soundFlechette.setBuffer(flechette);

    SoundBuffer fin;
    fin.loadFromFile(FEUX_ARTIFICE);
    Sound soundFin;
    soundFin.setBuffer(fin);

    /* SoundBuffer sonKamikaze;
     if (!sonKamikaze.loadFromFile(KAMI))
         printf("PB de chargement du son %s !\n", KAMI);

     SoundBuffer sonMonstreSimple;
     if (!sonMonstreSimple.loadFromFile(MONSTRES))
         printf("PB de chargement du son %s !\n", MONSTRES);

     SoundBuffer sonYeah;
     if (!sonYeah.loadFromFile(YEAH))
         printf("PB de chargement du son %s !\n", YEAH);

     //On declare qu'il y a du son
     //(soundBackground pour le vent et sound pour les ev�nements)
     Sound soundBackground, sound;*/

    SoundBuffer bufferVent;
    bufferVent.loadFromFile("Vent.wav");
    Sound soundVent;
    soundVent.setBuffer(bufferVent);
    soundVent.play();
    soundVent.setVolume(30);
    soundVent.setLoop(true);

    //Donner texture au joueur
    Texture perso,
            pommeT,
            murT,
            pommePoisonT;
    perso.loadFromFile(AVENTURIER);
    pommeT.loadFromFile(POMME);
    murT.loadFromFile(BRIQUE);
    pommePoisonT.loadFromFile(POMME_POISON);

    perso.setSmooth(true);
    pommeT.setSmooth(true);

    spriteAventurier.setTexture(perso);
    pomme.setTexture(pommeT);
    pommePoison.setTexture(pommePoisonT);
    spriteAventurier.setPosition(APPARITION_X_AVENTURIER,APPARITION_Y_AVENTURIER);

    //Les chronomètres pour le déplacement du perso et pour le chrono du jeu
    Clock clock,  //gestion temps réel
          chrono, //chrono score
          time; //animation perso

    chrono.restart();

    Time tempsEcoule,  //gestion temps réel
         tempsChrono = chrono.getElapsedTime(); // chrono score

    float dt = 0, //gestion temps reél
          scoreChrono = tempsChrono.asSeconds(); // chrono score


    int labyPetitMap[TAILLE_TABLEAU_Y][TAILLE_TABLEAU_X] =
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,4,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2},
        {1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,1,1,1,3,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,1,1,1,0,1,1,3,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,1},
        {1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1},
        {1,0,0,0,0,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1,1,5,1},
        {1,0,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,3,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1},
        {1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    std::vector<RectangleShape>vecLaby;
    std::vector<RectangleShape>vecMur;
    std::vector<RectangleShape>vecFin;
    std::vector<RectangleShape>vecPiege;
    for(int y = 0; y<TAILLE_TABLEAU_Y; y++)
    {
        for(int x = 0; x<TAILLE_TABLEAU_X; x++)
        {
            if(labyPetitMap[y][x] == 1)
            {
                RectangleShape mur(Vector2f(TAILLE_MUR,TAILLE_MUR));
                mur.setFillColor(Color(230,134,24));
                mur.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                mur.setTexture(&murT);
                vecLaby.push_back(mur);
                vecMur.push_back(mur);
            }
            else if(labyPetitMap[y][x] == 0)
            {
                RectangleShape sol(Vector2f(TAILLE_MUR,TAILLE_MUR));
                sol.setFillColor(Color(10,162,28));
                sol.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                vecLaby.push_back(sol);
            }
            else if(labyPetitMap[y][x] == 2)
            {
                RectangleShape fin(Vector2f(TAILLE_MUR,TAILLE_MUR));
                fin.setFillColor(Color(10,162,28));
                fin.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                vecFin.push_back(fin);
                vecLaby.push_back(fin);
            }
            else if(labyPetitMap[y][x] == 3)
            {
                RectangleShape piege(Vector2f(TAILLE_MUR,TAILLE_MUR));
                piege.setFillColor(Color(10,153,26));
                piege.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                vecPiege.push_back(piege);
                vecLaby.push_back(piege);
            }
            else if(labyPetitMap[y][x] == 4)
            {
                RectangleShape sol(Vector2f(TAILLE_MUR,TAILLE_MUR));
                sol.setFillColor(Color(10,162,28));
                sol.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                pomme.setPosition(x*TAILLE_MUR+TAILLE_MUR/2,y*TAILLE_MUR+TAILLE_MUR/4);
                vecLaby.push_back(sol);
            }
            else if(labyPetitMap[y][x] == 5)
            {
                RectangleShape sol(Vector2f(TAILLE_MUR,TAILLE_MUR));
                sol.setFillColor(Color(10,162,28));
                sol.setPosition(x*TAILLE_MUR,y*TAILLE_MUR);
                pommePoison.setPosition(x*TAILLE_MUR+TAILLE_MUR/4,y*TAILLE_MUR+TAILLE_MUR/2);
                vecLaby.push_back(sol);
            }
        }
    }
    while (app.isOpen())
    {
        // Process events
        Event event;
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
            // Escape pressed : exit
            if (event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
                app.close();
            //Le perso ne change pas si aucune touche pressée
            if(event.type == Event::KeyPressed)
                updateFPS = true;
            else
                updateFPS = false;
        }
        //}

        previous.x = aventurier.p.x;
        previous.y = aventurier.p.y;

        FloatRect spriteJoueur = spriteAventurier.getGlobalBounds();
        if (Keyboard::isKeyPressed( Keyboard::Left))
        {
            animation.y=Left;
            Vector2f test;
            test.x = aventurier.p.x+TAILLE_AVENTURIER;
            test.y = aventurier.p.y;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y][x-1] != 1){
                aventurier.p.x -= SPEED_AVENTURIER;
            }
        }

        if (Keyboard::isKeyPressed( Keyboard::Right))
        {
            animation.y=Right;
            Vector2f test;
            test.x = aventurier.p.x-TAILLE_AVENTURIER;
            test.y = aventurier.p.y;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y][x+1] != 1){
                aventurier.p.x += SPEED_AVENTURIER;
            }

        }

        if (Keyboard::isKeyPressed( Keyboard::Up))
        {
            animation.y=Up;

            Vector2f test;
            test.x = aventurier.p.x;
            test.y = aventurier.p.y+TAILLE_AVENTURIER;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y-1][x] != 1){
                aventurier.p.y -= SPEED_AVENTURIER;
            }
        }

        if (Keyboard::isKeyPressed( Keyboard::Down))
        {
            animation.y=Down;
            Vector2f test;
            test.x = aventurier.p.x;
            test.y = aventurier.p.y-TAILLE_AVENTURIER;
            int x = test.x/30;
            int y = test.y/30;
            if(labyPetitMap[y+1][x] != 1){
                aventurier.p.y += SPEED_AVENTURIER;
            }

        }
        //Pour que le decoupage de l'image soit bien
        //Et donne impression que le perso se deplace
        if (updateFPS==true)
        {
            if (time.getElapsedTime().asMilliseconds() >=70)
            {
                animation.x--;
                if (animation.x*TAILLE_AVENTURIER >= perso.getSize().x)
                    animation.x =2;
                time.restart();
            }
        }
        //Pour regeler qu'elle image est prise
        spriteAventurier.setTextureRect(IntRect(animation.x*TAILLE_AVENTURIER, animation.y*TAILLE_AVENTURIER, TAILLE_AVENTURIER, TAILLE_AVENTURIER));
        spriteAventurier.setPosition(aventurier.p.x,aventurier.p.y);


        //Mesure en temps reel
        tempsEcoule = clock.restart();
        dt = tempsEcoule.asSeconds();

        //draw de la barre de vie
        //Barre de vie

        RectangleShape overlayBarreVie(Vector2f(50,20));
        overlayBarreVie.setPosition(aventurier.p.x,aventurier.p.y-60);
        overlayBarreVie.setFillColor(Color::White);
        RectangleShape fondOverlay(Vector2f(40,10));
        fondOverlay.setPosition(aventurier.p.x+5,aventurier.p.y-55);
        fondOverlay.setFillColor(Color::Black);
        RectangleShape barreVie(Vector2f(aventurier.pv,10));
        barreVie.setPosition(aventurier.p.x+5,aventurier.p.y-55);
        barreVie.setFillColor(Color::Green);




        if(aventurier.pv<=20)
            barreVie.setFillColor(Color(222,120,31));
        if(aventurier.pv<=10)
            barreVie.setFillColor(Color::Red);

        if(!vecPiege.empty())
        {
            for(int i = 0; i < vecPiege.size(); i++)
            {
                if(spriteJoueur.intersects(vecPiege[i].getGlobalBounds()))
                {
                    soundFlechette.play();
                    aventurier.pv -= 10;
                    vecPiege.erase(vecPiege.begin()+i-1);
                }
            }

        }
        if(spriteJoueur.intersects(pomme.getGlobalBounds()))
        {
            pomme.setPosition(1500,0);
            if(aventurier.pv<=35)
            {
                aventurier.pv=aventurier.pv+5;
            }
            else
            {
                aventurier.pv=40;
            }

        }
        if(spriteJoueur.intersects(pommePoison.getGlobalBounds()))
        {
            pommePoison.setPosition(1500,0);
            aventurier.pv -= 20;

        }


        //détéection de l'arrivée
        if(spriteJoueur.intersects(vecFin[0].getGlobalBounds()))
        {
            soundVent.stop();
            if(soundFin.getStatus() == SoundSource::Stopped)
            {
                soundFin.play();
            }

            //mettre la fin
        }

        // Clear screen
        app.clear();

        // Draw the sprite

        for(int i=0; i < vecLaby.size(); i++)
        {
            app.draw(vecLaby[i]);
        }

        View view1(Vector2f(aventurier.p.x,aventurier.p.y),Vector2f(300.f, 200.f));
        app.setView(view1);
        app.draw(pomme);
        app.draw(pommePoison);
        app.draw(spriteAventurier);
        app.draw(overlayBarreVie);
        app.draw(fondOverlay);
        app.draw(barreVie);
        // Update the window
        app.display();
    }
    return EXIT_SUCCESS;
}
